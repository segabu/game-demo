﻿
//Class for question
//Uses System.Serializable to able editing in Unity
[System.Serializable]
public class Question
{
    //Attributes edited in Unity
    public string question;
    public string explanation;
    public Pair Answer1;
    public Pair Answer2;
    public Pair Answer3;
    public Pair Answer4;

    //Nested Pair class to tie answer and value
    [System.Serializable]
    public class Pair
    {
        //Attributes edited in Unity
        public string answer;
        public int value;
    }
}
