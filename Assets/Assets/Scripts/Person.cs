﻿using UnityEngine;
using System.Collections;

//Person class to parse JSON. 
//Parser uses System.Serializable
[System.Serializable]
public class Person {

    public string _id;
    public string firstName;
    public string userName;
    public string lastName;
    public string email;
    public string password;
    public string points;
    public string level;
    public string passwordConfirm;

    /// <summary>
    /// Method to parse JSON
    /// <see cref="JsonUtility.FromJson"></see>
    /// </summary>
    /// <param name="jsonString">JSON from server</param>
    /// <returns>Parsed Person object</returns>
    public static Person CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Person>(jsonString);
    }

    public static string ToJSON(Person p)
    {
        return JsonUtility.ToJson(p);
    }

    public static string pointsToJson(Person p)
    {
        return JsonUtility.ToJson(p.points);
    }

}
