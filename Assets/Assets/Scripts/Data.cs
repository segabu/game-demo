﻿using UnityEngine;
using System.Collections;

//Class to store person data over scenes
public class Data : MonoBehaviour{

    
    private Person person;

    //set;get;
    public void setPerson(Person p)
    {
        this.person = p;
    }
    public Person getPerson()
    {
        return this.person;
    }
}
