﻿using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Linq;

public class Controller : MonoBehaviour {

    //Attributes in Unity
    public Question[] questions;
    public Text questionText;
    public Text choice1;
    public Text choice2;
    public Text choice3;
    public Text choice4;
    public Text scoreText;
    public Button[] buttons;

    //Private attributes of the class
    private static List<Question> notAnswered;
    private int score;
    private float nextQuestionWait = 1f;
    private float greetingWait = 6f;
    private Question current;
    private bool answered;
    private GameObject data;
    private Data personData;
    private Person person;
    private string personJson;
    private bool atStart;
    private bool noInfo;

    /// <summary>
    /// Initializes the game
    /// </summary>
    void Start()
    {
        data = GameObject.FindGameObjectWithTag("DataObject");
        personData = (Data)data.GetComponent(typeof(Data));
        person = personData.getPerson();
        score = 0;
        UpdateScore();
        CleanSelection();
        atStart = true;
        noInfo = true;
        //If list is empty
        if (notAnswered == null || notAnswered.Count == 0)
        {
            notAnswered = questions.ToList<Question>(); 
        }
        
        StartCoroutine(NextQuestion());
    }
    IEnumerator Upload()
    {
        UnityWebRequest www = UnityWebRequest.Put("https://segabu2.herokuapp.com/contacts/"+person._id, personJson);
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.Send();
        if (www.isError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log("Upload complete");
        }
    }
    
    
    /// <summary>
    /// Method for changing the questions displayed for user
    /// </summary>
    /// <returns>Time between questions</returns>
    IEnumerator NextQuestion()
    {
        UpdateScore();
        disableButtons();
        //Greet the person
        if (atStart == true)
        {
            questionText.text = "Hello " + person.firstName + ". Welcome to the interview!";
            yield return new WaitForSeconds(greetingWait);
            atStart = false;
        }
        if(!noInfo)
        {
            CleanSelection();
            ShowInformation();
            yield return new WaitForSeconds(greetingWait);
        }
        //Wait between questions
        yield return new WaitForSeconds(nextQuestionWait);
        if (notAnswered.Count > 0)
        {
            GetQuestion();
            ShowQuestion();
        }
        else
        {
            if(score >= 500)
            {
                int points = System.Int32.Parse(person.points);
                //Debug.Log(points);
                person.points = points.ToString();
                UploadStats();
            }
            CleanSelection();

            //Set game over text
            questionText.text = "Well done! You got "+score+" points";
        }
    }

    /// <summary>
    /// Method in Controller class.
    /// <para>Úsers choice from the shown alternatives</para>
    /// </summary>
    /// <param name="c">Button.OnClick()</param>
    public void Selection(int c)
    {
        switch (c)
        {
            case 1:
                score += current.Answer1.value;
                break;
            case 2:
                score += current.Answer2.value;
                break;
            case 3:
                score += current.Answer3.value;
                break;
            case 4:
                score += current.Answer4.value;
                break;
        }

        noInfo = false;
        StartCoroutine(NextQuestion());
    }

    //Private methods
    /// <summary>
    /// Method to get the next questions from the existing lists
    /// </summary>
    private void GetQuestion()
    {
        int num = Random.Range(0, notAnswered.Count);
        current = notAnswered[num];
        notAnswered.RemoveAt(num);
    }

    /// <summary>
    /// Uses Question class to set current questions for user
    /// </summary>
    private void ShowQuestion()
    {
        questionText.text = current.question;
        choice1.text = current.Answer1.answer;
        choice2.text = current.Answer2.answer;
        choice3.text = current.Answer3.answer;
        choice4.text = current.Answer4.answer;
        activateButtons();

    }
    /// <summary>
    /// Uses Question class to set information about the questions
    /// </summary>
    private void ShowInformation()
    {
        questionText.text = current.explanation;
    }

    /// <summary>
    /// Method in Controller class to update the player score
    /// </summary>
    private void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    /// <summary>
    /// Method to set selection and text to empty
    /// </summary>
    private void CleanSelection()
    {
        choice1.text = "";
        choice2.text = "";
        choice3.text = "";
        choice4.text = "";
        
    }
    /// <summary>
    /// Method to start upload
    /// </summary>
    private void UploadStats()
    {
        personJson = Person.ToJSON(person); 
        Debug.Log(personJson);
        StartCoroutine(Upload());
    }

    /// <summary>
    /// Method to active buttons
    /// </summary>
    private void activateButtons()
    {
        foreach (Button b in buttons)
        {
            b.interactable = true;
        }
    }

    /// <summary>
    /// Method to disable buttons
    /// </summary>
    private void disableButtons()
    {
        foreach (Button b in buttons)
        {
            b.interactable = false;
        }
    }
}
