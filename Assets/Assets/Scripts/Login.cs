﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Login : MonoBehaviour{

    //InputFields from Unity
    public InputField username;
    public InputField password;
    
    //Attributes for class usage
    private GameObject data;
    private string un;
    private string pw;
    private Data person;
    private Person personData;

    /// <summary>
    /// Initializes the Login scene
    /// </summary>
    void Start()
    {
        data = GameObject.FindWithTag("DataObject");
        person = (Data) data.GetComponent(typeof(Data));
        pw = "";
        un = "";
    }

    /// <summary>
    /// Update changes from Login scene
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (username.isFocused)
            {
                password.Select();
            }
        }
        un = username.text;
        pw = password.text;
    }

    /// <summary>
    /// Downloads user data from the server
    /// </summary>
    /// <returns>Response wait</returns>
    IEnumerator LoginPressed()
    {
        //Creates UnityWebRequest to get data from server.
        using (UnityWebRequest www = UnityWebRequest.Get("https://segabu2.herokuapp.com/contacts/user/"+un))
        {
            //Send the request
            yield return www.Send();
            if (www.isError)
            {
                Debug.Log(www.error);
            }
            else
            {
                //Parse user data
                personData = Person.CreateFromJSON(www.downloadHandler.text);
                person.setPerson(personData);
                Object.DontDestroyOnLoad(GameObject.Find("DataObject"));
                SceneManager.LoadScene("Main", LoadSceneMode.Single);
            }
        }
        
    }

    /// <summary>
    /// When Login button pressed. Download is started.
    /// </summary>
    public void PressLogin()
    {
        StartCoroutine(LoginPressed());
    }
}
